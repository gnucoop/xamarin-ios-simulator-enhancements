﻿/* <copyright file="ApiDefinition.cs" company="gnucoop">
 * 
 * Copyright (c) 2014 Gnucoop. http://www.gnucop.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

using System;
using System.Drawing;

using MonoTouch.ObjCRuntime;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreLocation;

namespace XamarinSimulatorEnhancements
{
	/// <summary>
	/// Binding to CESimulatorEnhancements class
	/// </summary>
	[BaseType (typeof(NSObject))]
	interface CESimulatorEnhancements {
		//
		// - (void)enable;
		//
		/// <summary>
		/// Enable the simulator enhancements
		/// </summary>
		[Export("enable")]
		void Enable();

		//
		// - (void)startClient;
		//
		/// <summary>
		/// Starts the client that polls for location and accelerometer data from Simulator Enhancements Server
		/// </summary>
		[Export("startClient")]
		void StartClient();
	}
}

