#Xamarin iOS Simulator Enhancements

A Xamarin (MonoTouch) port of the great [Simulator Enhancements by ColinEberhardt][original_repo]

[original_repo]: https://github.com/ColinEberhardt/SimulatorEnhancements/ "Simulator Enhancements by ColinEberhardt"

This project provides a mechanism for 'injecting' sensor data into an application in order to test code that depends on the accelerometer or GPS data without the need for a real device.

![SimulatorEnhancementsImage](https://bytebucket.org/gnucoop/xamarin-ios-simulator-enhancements/raw/721786e8a4c7112a6a99d12b18da86b03fd3d1ef/SimulatorEnhancements.jpg "Simulator Enhancements")""

The steps to get iOS Simulator Enhancements working are:
	
1. Modifiy Main.cs and add the following lines in Main method 

		#if DEBUG
		CESimulatorEnhancements simEn = new CESimulatorEnhancements ();	
		simEn.Enable ();
		simEn.StartClient ();
		#endif

	There is a sample code in Main.cs.txt

2. Once this is done, the app will poll localhost/data requesting the current, simulated, location and accelerometer data.

	To simulate this data, use the [SimulatorEnhancements-Server project][server_repo], which is a simple web server that provides data in the following format:

[server_repo]: https://github.com/trik/SimulatorEnhancements-Server "SimulatorEnhancements-Servct"

#Installation

Precompiled binaries are available as [NuGet package] [nuget_pkg].

	Install-Package Xamarin.iOS.Simulator.Enhancements

[nuget_pkg]: https://www.nuget.org/packages/Xamarin.iOS.Simulator.Enhancements/